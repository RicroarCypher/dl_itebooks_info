#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
dl_itebooks_info.py
"""
__author__ = 'Christopher W. Carter'
__email__ = 'Ricroar.Cypher@gmail.com'
__license__ = "Copyright 2015 : All Rights Reserved"

import argparse
#~ import logging
import sys

import urllib.request
import http.cookiejar
import os.path
import shutil

from bs4 import BeautifulSoup

class dl_itebooks_info(object):
	"""
	dl_itebooks_info
	"""
	def __init__(self):
		self.url_base = r'http://it-ebooks.info/'
		self.user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0'
		self.headers = { 'User-Agent' : self.user_agent }
		
		self.ebook = {}
		
	def getEBookInfo(self, id):
		"""Downloads information about EBook"""
		
		url = self.url_base + r'book/' + str(id) + r'/'

		data = None
		req = urllib.request.Request(url, data=data, headers=self.headers)
		response = urllib.request.urlopen(req)

		soup = BeautifulSoup(response)
		self.ebook['id'] = str(id)
		self.ebook['title'] = soup.select('h1[itemprop="name"]')[0].contents[0]
		self.ebook['publisher'] = soup.select('a[itemprop="publisher"]')[0].contents[0]
		self.ebook['isbn'] = soup.select('b[itemprop="isbn"]')[0].contents[0]
		self.ebook['published'] = soup.select('b[itemprop="datePublished"]')[0].contents[0]
		self.ebook['link'] = soup.select('a[href^="http://filepi.com/i/"]')[0]['href']
		self.ebook['format'] = soup.select('b[itemprop="bookFormat"]')[0].contents[0]
		self.ebook['language'] = soup.select('b[itemprop="inLanguage"]')[0].contents[0]
		self.ebook['pages'] = soup.select('b[itemprop="numberOfPages"]')[0].contents[0]
		self.ebook['author'] = soup.select('b[itemprop="author"]')[0].contents[0]
		self.ebook['description'] = soup.select('span[itemprop="description"]')

	def getEBook(self, url):
		"""Downloads the EBook, renames it, and moves it"""
		data = None
		self.headers['Referer'] = r'http://it-ebooks.info/book/' + self.ebook['id']
		file_name = ''.join([
				'[', self.ebook['publisher'], ']',
				self.ebook['title'],
				' (', self.ebook['published'], ')',
				'.', self.ebook['format'].lower()
			])
		with urllib.request.urlopen(urllib.request.Request(url, data=data, headers=self.headers)) as response, open(file_name, 'wb') as out_file:
			#~ out_file.write(response.read())
			print('Downloading "' + file_name + '"... ', end='')
			#~ shutil.copyfileobj(response, out_file)
			print('Done.')
	def writeISBNFile(self):
		"""Writes a .txt file containing the ISBN information"""
		file_name = ''.join([
				'[', self.ebook['publisher'], ']',
				self.ebook['title'],
				' (', self.ebook['published'], ')',
				'_ISBN.txt'
			])
		with open(file_name, 'wt') as out_file:
			out_file.write(self.ebook['isbn'])

	#Run Function:  ######################################################
	def run(self, start=1, stop=None, step=-1):
		"""Basic functionality of script."""
		if stop is None:
			stop = int(start) - 1

		for i in (x for x in range(int(start), int(stop), step)):
			self.getEBookInfo(i)
			self.getEBook(self.ebook['link'])
			#~ self.writeISBNFile()

# Parse Arguments:  ####################################################
def parseArgs(args):
    """ Parse the command-line arguments. """

    parser = argparse.ArgumentParser(
        description=''
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Print debug information',
        action='store_true', default=False
    )
    parser.add_argument(
        '-s', '--start',
        help='Starting index',
        type=int, default=1
    )
    parser.add_argument(
        '-f', '--stop',
        help='Stopping index',
        type=int, default=None
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--increment',
        help='Increments from the starting index',
        action='store_true', default=False
    )
    group.add_argument(
        '--decrement',
        help='Decrements from the starting index (default)',
        action='store_true', default=False
    )
    return parser.parse_args(args)

# Main:  ###############################################################
def main():
	"""Parses arguments, initializes class, and returns class.run()."""

	args = parseArgs(sys.argv[1:])

	#~ if args.verbose:
		#~ logging.basicConfig(
			#~ format='%(levelname)s %(filename)s: %(message)s',
			#~ level=logging.DEBUG
		#~ )
	#~ else:
		#~ # Log info and above to console
		#~ logging.basicConfig(
			#~ format='%(levelname)s: %(message)s',
			#~ level=logging.INFO
		#~ )
	if args.decrement is True:
		step = -1
	elif args.increment is True:
		step = 1
	else:
		step = -1
	mod = dl_itebooks_info()
	mod.run(args.start, args.stop, step)

if __name__ == '__main__':
	main()
